# 

Example of a HTML, CSS and JavaScript dashboard communicating with thingspeak's API

# dashboard_thingspeak

This project contains an example of a dashboard used to communicate with the api at: https://thingspeak.com/channels/731067

The example uses HTML, CSS and JavaScript as well as some 3rd party libraries.

## Used 3rd party libs 

    * JQuery https://jquery.com/ 
    * Bootstrap-material-design https://fezvrasta.github.io/bootstrap-material-design/
    * chart.js https://www.chartjs.org/
    * Fetch API (included in JQuery) https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API