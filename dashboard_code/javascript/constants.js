let baseUrl = 'https://api.thingspeak.com/channels/731067/feeds.json' //example: https://api.thingspeak.com/channels/731067/feeds.json?results=2
let FeedUpdateUrl = 'https://api.thingspeak.com/update';
let fieldUrl = 'https://api.thingspeak.com/channels/731067/fields/';
let writeApiKey = ''; //insert you thingspeak api write key here
let field4 = '4'
let field5 = '5'

export{baseUrl, FeedUpdateUrl, fieldUrl, writeApiKey, field4, field5}