import {baseUrl} from './constants.js'
import {addData, config} from './chart.js'

async function httpGet(url){
    try {
        let response = await fetch(url, {
            method: 'GET'
        });
        updateHttpLog(`${response.status} ${response.statusText}`);
        let json = await response.json();
        return json;
    } catch (error) {
        console.error(error);
        updateHttpLog(error);
    }
}

async function httpPost(url, data) {
    try {
        let response = await fetch(url, {
            method: 'POST', // 'GET', 'PUT', 'DELETE', etc.
            body: JSON.stringify(data), // Coordinate the body type with 'Content-Type'
            headers: new Headers({
                'Content-Type': 'application/json;charset=UTF-8'
            })
        });
        updateHttpLog(`${response.status} ${response.statusText}`);
        return response;
    }
    catch (error) {
        console.error(error);
        updateHttpLog(error);
    }
}

async function updateGraph() {
    // Get latest values from feed, put in variable getResponse
    let getResponse = await httpGet(baseUrl+'?results=1'); 
    //Destructure getResponse
    const {field1: temperature, field2: humidity, field3: pressure} = getResponse.feeds[0];
    //Add data to graphs
    addData(window.iot_graphs, curTime(), [temperature, humidity, pressure]); 
};

function updateHttpLog(text)
{
    let time = curTime();
    let li = document.createElement("li");
    li.appendChild(document.createTextNode(`${time} ${text}`));
    $('#api-log').prepend(li);
}

async function getChannelFeed(url) {
    // Get latest values from feed, put in variable getResponse
    let getResponse = await httpGet(url+'?results=10');
    const {field1: temperature, field2: humidity, field3: pressure, field4: buttonCard, field5: switchCard, field6: inputField} = getResponse.feeds[0];
    return [temperature, humidity, pressure, buttonCard, switchCard, inputField];
}

async function getChannelField(url, fieldNumber){
    //https://api.thingspeak.com/channels/731067/fields/1.json?results=2
    let updateRequest = `${url}${fieldNumber}.json?results=1`
    let getResponse = await httpGet(updateRequest);
    return getResponse.feeds[0];
}

async function updateThingSpeakField(baseUrl, writeApiKey, field, data){
    let updateRequest = `${baseUrl}?api_key=${writeApiKey}&field${field}=${data}`
    let getResponse = await httpGet(updateRequest);
    
    if (getResponse != 0) {
        return `field ${field} updated`
    }
    else {
        return `field ${field} NOT updated`
        
    }
}

function initGraph(id){
    var ctx = document.getElementById(id).getContext('2d');
    window.iot_graphs = new Chart(ctx, config);
}

function updateStatusField(data, statusField){   
    statusField.html(data);
    parseInt(data) ? statusField.css('backgroundColor', 'darkolivegreen') : statusField.css('backgroundColor', 'tomato');
}

//GET TIME
function curTime (){
    let now = new Date().toLocaleTimeString('da-DK');
    return now;
}

export {httpGet, updateGraph, updateHttpLog, curTime, getChannelFeed, getChannelField, updateThingSpeakField, initGraph, updateStatusField}