import {updateGraph, updateHttpLog, getChannelField, updateThingSpeakField, initGraph, updateStatusField} from './functions.js'
import {FeedUpdateUrl, fieldUrl, writeApiKey, field4, field5} from './constants.js'


$(document).ready(function(){

    let onButton = $('#on-btn');
    let offButton = $('#off-btn');
    let switch1 = $('#switch1');
    let updateButton = $('#update-btn');
    let sendButton = $('#send-btn');
    let statusField1 = $('#status-field-1');
    let statusField3 = $('#status-field-3');
    let slider1 = $('#slider1');

    initGraph("myChart");
    statusField3.html(slider1.val())

    //When on button is clicked    
    onButton.click(async () => {
        let updateResponse = await updateThingSpeakField(FeedUpdateUrl, writeApiKey, field4, '1');
        updateHttpLog(updateResponse);
        let channelFieldResponse = await getChannelField(fieldUrl, field4);
        if (channelFieldResponse.field4 != null) {
            updateStatusField(channelFieldResponse.field4.toString(), statusField1);        
        }
    });

    offButton.click(async () => {
        let updateResponse = await updateThingSpeakField(FeedUpdateUrl, writeApiKey, field4, '0');
        updateHttpLog(updateResponse);
        let channelFieldResponse = await getChannelField(fieldUrl, field4);
        if (channelFieldResponse.field4 != null) {
            updateStatusField(channelFieldResponse.field4.toString(), statusField1);        
        }
    });

    switch1.on('change', () => {
        //execute your function here
    });

    slider1.on('input', () => {
        statusField3.html(slider1.val())
    })

    sendButton.click(async () => {
        let sliderValue = slider1.val();
        let updateResponse = await updateThingSpeakField(FeedUpdateUrl, writeApiKey, field5, sliderValue);
        updateHttpLog(updateResponse);
    });
    
    // UPDATE GRAPH MANUALLY
    updateButton.click(updateGraph);
    
    // UPDATE GRAPH IN MS INTERVALS
    setInterval(updateGraph, 20000);
});